from ROOT import *
import math
import sys
def main(argv):
    RATIO = "kkk"
    SCALE = "ppp"
    gROOT.LoadMacro("AtlasStyle.C")
    SetAtlasStyle()
    gROOT.SetBatch(1)
    gStyle.SetLineWidth(1)
    gStyle.SetMarkerSize(1)
    gStyle.SetOptStat(False)
    gStyle.SetPaintTextFormat("1.4f");
    gStyle.SetErrorX(0) 
    WP = argv[0]
    Year = argv[1]
    Variation = 'nominal'#argv[2]
    nominalDATA  = TFile("/afs/in2p3.fr/home/h/hxu/QMISID/EGRateMeasure/For_egamma_Truth/All"+Year+"_"+Variation+"/Output_DATA_"+WP+".root",'READ')
    nominalMC  = TFile("/afs/in2p3.fr/home/h/hxu/QMISID/EGRateMeasure/For_egamma_Truth/All"+Year+"_"+Variation+"/Output_MC_"+WP+".root",'READ')
    
    etas = [0.0, 0.6, 1.1, 1.52, 1.7, 2.3, 2.5]
    pts  = [25.0,30.0,40.0,50.0,60.0,70.0, 95.0, 130.0, 1000.0]


    #Nonimal SF and STAT
   
    h_nominalDATA  =  nominalDATA.Get("80.0_100.0_0.0_0.0_DATA_"+WP+"_Correctedmisid").Clone()
    h_nominalMC  =  nominalMC.Get("80.0_100.0_0.0_0.0_MC_"+WP+"_Correctedmisid").Clone()
  
    SFnominal = h_nominalDATA.Clone()
    SFnominal.Divide(h_nominalMC)

    SFstat = SFnominal.Clone()
    for i in range(1,7):
       SFnominal.GetYaxis().SetBinLabel(i,str(etas[i-1])+"-"+str(etas[i]))
       SFstat.GetYaxis().SetBinLabel(i,str(etas[i-1])+"-"+str(etas[i]))
       for j in range(1,9):
          SFstat.SetBinContent(j,i,SFnominal.GetBinError(j,i))
    for j in range(1,9):
       SFnominal.GetXaxis().SetBinLabel(j,str(pts[j-1])+"-"+str(pts[j]))
       SFstat.GetXaxis().SetBinLabel(j,str(pts[j-1])+"-"+str(pts[j]))
    c1 = TCanvas("c1","c1",1000,750)
    c1.SetRightMargin(5)
    SFnominal.GetXaxis().SetTitle("P_{T}[GeV]")
    SFnominal.Draw("colz")
    
    SFnominal.Draw("textsame")
    SFnominal.SetBins(8,0,8,6,0,6)
    
    #SFnominal.GetXaxis().SetRangeUser(25,130)
    c1.Print(WP+"_SF_nominal_"+Year+".pdf")
    SFstat.SetBins(8,0,8,6,0,6)
    SFstat.Divide(SFnominal)
    SFstat.Draw("colz")
    SFstat.Draw("textsame")
    c1.Print(WP+"_SF_Stat_"+Year+".pdf")
    
    #First Variation bkg sub
    Variation = 'bkgsub'  
    bkgsubDATA =  TFile("/afs/in2p3.fr/home/h/hxu/QMISID/EGRateMeasure/For_egamma_Truth/All"+Year+"_"+Variation+"/Output_DATA_"+WP+".root",'READ')
    bkgsubMC = TFile("/afs/in2p3.fr/home/h/hxu/QMISID/EGRateMeasure/For_egamma_Truth/All"+Year+"_"+Variation+"/Output_MC_"+WP+".root",'READ')
    h_bkgsubDATA = bkgsubDATA.Get("80.0_100.0_20.0_20.0_DATA_"+WP+"_Correctedmisid").Clone()
    h_bkgsubMC = bkgsubMC.Get("80.0_100.0_20.0_20.0_MC_"+WP+"_Correctedmisid").Clone()
    SFbkgsub = h_bkgsubDATA.Clone()
    SFbkgsub.Divide(h_bkgsubMC)
    for i in range(1,7):
       SFbkgsub.GetYaxis().SetBinLabel(i,str(etas[i-1])+"-"+str(etas[i]))
    for j in range(1,9):
       SFbkgsub.GetXaxis().SetBinLabel(j,str(pts[j-1])+"-"+str(pts[j]))
    SFbkgsub.SetBins(8,0,8,6,0,6)
    SFbkgsub.Add(SFnominal,-1.)
    SFbkgsub.Divide(SFnominal)
    for i in range(1,7):
       for j in range(1,9):
         SFbkgsub.SetBinContent(j,i,math.fabs(SFbkgsub.GetBinContent(j,i)))
    SFbkgsub.Draw("colz")
    SFbkgsub.Draw("textsame")
    c1.Print(WP+"_SF_bkgsub_"+Year+".pdf")
   
    #Truth

    TruthMC =  TFile("/afs/in2p3.fr/home/h/hxu//QMISID/EGRateMeasure/For_egamma_Truth/MCLHvariation/All"+Year+"_LH/Output_mc"+Year+"_"+WP+".root")
    h_truthMC = TruthMC.Get("80.0_100.0_0.0_0.0_mc"+Year+"_"+WP+"_Correctedmisid").Clone()
    
    SFTruth  =  nominalDATA.Get("80.0_100.0_0.0_0.0_DATA_"+WP+"_Correctedmisid").Clone()
    SFTruth.Divide(h_truthMC)
    SFTruth.SetBins(8,0,8,6,0,6)
    SFTruth.Add(SFnominal,-1.)
    SFTruth.Divide(SFnominal)
    for i in range(1,7):
       SFTruth.GetYaxis().SetBinLabel(i,str(etas[i-1])+"-"+str(etas[i]))
    for j in range(1,9):
       SFTruth.GetXaxis().SetBinLabel(j,str(pts[j-1])+"-"+str(pts[j]))
    for i in range(1,7):
       for j in range(1,9):
         SFTruth.SetBinContent(j,i,math.fabs(SFTruth.GetBinContent(j,i)))

    SFTruth.Draw("colz")
    SFTruth.Draw("textsame")
    c1.Print(WP+"_SF_Truth_"+Year+".pdf")
    

    #Mass

    Variation = 'mass'
    massDATA =  TFile("/afs/in2p3.fr/home/h/hxu/QMISID/EGRateMeasure/For_egamma_Truth/All"+Year+"_"+Variation+"/Output_DATA_"+WP+".root",'READ')
    massMC = TFile("/afs/in2p3.fr/home/h/hxu/QMISID/EGRateMeasure/For_egamma_Truth/All"+Year+"_"+Variation+"/Output_MC_"+WP+".root",'READ')
    h_massDATA = massDATA.Get("70.0_110.0_0.0_0.0_DATA_"+WP+"_Correctedmisid").Clone()
    h_massMC = massMC.Get("70.0_110.0_0.0_0.0_MC_"+WP+"_Correctedmisid").Clone()
    SFmass = h_massDATA.Clone()
    SFmass.Divide(h_massMC)
    for i in range(1,7):
       SFmass.GetYaxis().SetBinLabel(i,str(etas[i-1])+"-"+str(etas[i]))
    for j in range(1,9):
       SFmass.GetXaxis().SetBinLabel(j,str(pts[j-1])+"-"+str(pts[j]))
    SFmass.SetBins(8,0,8,6,0,6)
    SFmass.Add(SFnominal,-1.)
    SFmass.Divide(SFnominal)
    for i in range(1,7):
       for j in range(1,9):
         SFmass.SetBinContent(j,i,math.fabs(SFmass.GetBinContent(j,i)))
    SFmass.Draw("colz")
    SFmass.Draw("textsame")
    c1.Print(WP+"_SF_mass_"+Year+".pdf")
    

    #trigger 
    Variation = 'trigger'
    triggerDATA =  TFile("/afs/in2p3.fr/home/h/hxu/QMISID/EGRateMeasure/For_egamma_Truth/All"+Year+"_"+Variation+"/Output_DATA_"+WP+".root",'READ')
    triggerMC = TFile("/afs/in2p3.fr/home/h/hxu/QMISID/EGRateMeasure/For_egamma_Truth/All"+Year+"_"+Variation+"/Output_MC_"+WP+".root",'READ')
    h_triggerDATA = triggerDATA.Get("80.0_100.0_0.0_0.0_DATA_"+WP+"_Correctedmisid").Clone()
    h_triggerMC = triggerMC.Get("80.0_100.0_0.0_0.0_MC_"+WP+"_Correctedmisid").Clone()
    SFtrigger = h_triggerDATA.Clone()
    SFtrigger.Divide(h_massMC)
    for i in range(1,7):
       SFtrigger.GetYaxis().SetBinLabel(i,str(etas[i-1])+"-"+str(etas[i]))
    for j in range(1,9):
       SFtrigger.GetXaxis().SetBinLabel(j,str(pts[j-1])+"-"+str(pts[j]))
    SFtrigger.SetBins(8,0,8,6,0,6)
    SFtrigger.Add(SFnominal,-1.)
    SFtrigger.Divide(SFnominal)
    for i in range(1,7):
       for j in range(1,9):
         SFtrigger.SetBinContent(j,i,math.fabs(SFtrigger.GetBinContent(j,i)))
    SFtrigger.Draw("colz")
    SFtrigger.Draw("textsame")
    c1.Print(WP+"_SF_trigger_"+Year+".pdf")


    return
main(sys.argv[1:])

