rm -rf Result${1}
mkdir Result${1}

python Final.py TightT  ${1}
python Final.py TightTT ${1}
python Final.py TightG ${1}
python Final.py TightGL ${1}
python Final.py MediumT ${1}
python Final.py MediumTT ${1}
python Final.py MediumG ${1}
python Final.py MediumGL ${1}
python Final.py Tight ${1}
python Final.py Medium ${1}
python Final.py LooseBL ${1}
mv *${1}.pdf Result${1}
