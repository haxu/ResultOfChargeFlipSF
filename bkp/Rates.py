from ROOT import *
import math
import numpy as np
from array import array
def main():
    RATIO = "kkk"
    SCALE = "ppp"
    gROOT.LoadMacro("AtlasStyle.C")
    SetAtlasStyle()
    gROOT.SetBatch(1)
    gStyle.SetLineWidth(1)
    gStyle.SetPaintTextFormat("1.4f");
    gStyle.SetMarkerSize(1)
    gStyle.SetOptStat(False)
    inputfile1  = TFile("/afs/in2p3.fr/home/h/hxu/QMISID/EGRateMeasure/For_egamma_Truth/All17/Output_DATA_TightT.root",'READ')
    inputfile2  = TFile("/afs/in2p3.fr/home/h/hxu/QMISID/EGRateMeasure/For_egamma_Truth/All17/Output_MC_TightT.root",'READ')
    inputfile3  = TFile("/afs/in2p3.fr/home/h/hxu/QMISID/EGRateMeasure/For_egamma_Truth/All17/80.000000_100.000000_0.000000_0.000000_DATA_TightT.root")
    inputfile4  = TFile("/afs/in2p3.fr/home/h/hxu/QMISID/EGRateMeasure/For_egamma_Truth/All17/80.000000_100.000000_0.000000_0.000000_MC_TightT.root")
    
    inputRawRateDATA = inputfile3.Get("80.0_100.0_0.0_0.0_DATA_TightT_misid").Clone()
    inputRawRateMC = inputfile4.Get("80.0_100.0_0.0_0.0_MC_TightT_misid").Clone()
  
    inputCorrectedRateDATA = inputfile1.Get("80.0_100.0_0.0_0.0_DATA_TightT_Correctedmisid").Clone()
    inputCorrectedRateMC = inputfile2.Get("80.0_100.0_0.0_0.0_MC_TightT_Correctedmisid").Clone()
    etas = [0.0, 0.6, 1.1, 1.52, 1.7, 2.3, 2.5]
    pts  = [25.0,30.0,40.0,50.0,60.0,70.0, 95.0, 130.0, 1000.0]
    #etaArr, ptArr = array('d',etas), array('d',pts)
    RawRateDATA = TH2F("RawRateDATA", ";P_{T}[GeV];#eta",8, 0,8, 6,0,6)
    RawRateMC = TH2F("RawRateMC", ";P_{T}[GeV];#eta", 8, 0,8, 6,0,6)
    CorrectedRateDATA = TH2F("CorrectedRateDATA", ";P_{T}[GeV];#eta",8, 0,8, 6,0,6)
    CorrectedRateMC = TH2F("CorrectedRateMC", ";P_{T}[GeV];#eta",8, 0,8, 6,0,6)
    
    RawRateDATA1D = TH1F("RawRateDATA1D", ";P_{T} Bin in different #eta range;Rates",48,0,48)
    RawRateMC1D = TH1F("RawRateMC1D", ";Bin;Rates", 48,0,48)
    CorrectedRateDATA1D = TH1F("CorrectedRateDATA1D",";Bin;Rates",48,0,48)
    CorrectedRateMC1D = TH1F("CorrectedRateMC1D", ";Bin;Rates",48,0,48)
    for i in range(1,7):
       for j in range(1,9):
             RawRateDATA.SetBinContent(j,i,inputRawRateDATA.GetBinContent(i,j))
             RawRateDATA.SetBinError(j,i,inputRawRateDATA.GetBinError(i,j))
             CorrectedRateDATA.SetBinContent(j,i,inputCorrectedRateDATA.GetBinContent(j,i))
             CorrectedRateDATA.SetBinError(j,i,inputCorrectedRateDATA.GetBinError(j,i))
             
             RawRateMC.SetBinContent(j,i,inputRawRateMC.GetBinContent(i,j))
             RawRateMC.SetBinError(j,i,inputRawRateMC.GetBinError(i,j))
             CorrectedRateMC.SetBinContent(j,i,inputCorrectedRateMC.GetBinContent(j,i))
             CorrectedRateMC.SetBinError(j,i,inputCorrectedRateMC.GetBinError(j,i))
             
             RawRateMC1D.SetBinContent(j+(i-1)*8,inputRawRateMC.GetBinContent(i,j))
             RawRateMC1D.SetBinError(j+(i-1)*8,inputRawRateMC.GetBinError(i,j))
             RawRateDATA1D.SetBinContent(j+(i-1)*8,inputRawRateDATA.GetBinContent(i,j))
             RawRateDATA1D.SetBinError(j+(i-1)*8,inputRawRateDATA.GetBinError(i,j))

             CorrectedRateMC1D.SetBinContent(j+(i-1)*8,inputCorrectedRateMC.GetBinContent(j,i))
             CorrectedRateMC1D.SetBinError(j+(i-1)*8,inputCorrectedRateMC.GetBinError(j,i))
             CorrectedRateDATA1D.SetBinContent(j+(i-1)*8,inputCorrectedRateDATA.GetBinContent(j,i))
             CorrectedRateDATA1D.SetBinError(j+(i-1)*8,inputCorrectedRateDATA.GetBinError(j,i))
    #etas = [0.0, 0.6, 1.1, 1.52, 1.7, 2.3, 2.5]
    #pts  = [20.0,30.0,40.0,50.0,60.0,70.0, 95.0, 130.0, 1000.0]
    #CorrectedRateDATA.GetYaxis().LabelsOption("d")    
    for i in range(1,7):
       CorrectedRateDATA.GetYaxis().SetBinLabel(i,str(etas[i-1])+"-"+str(etas[i])) 
       RawRateMC.GetYaxis().SetBinLabel(i,str(etas[i-1])+"-"+str(etas[i]))
       CorrectedRateMC.GetYaxis().SetBinLabel(i,str(etas[i-1])+"-"+str(etas[i]))
       RawRateDATA.GetYaxis().SetBinLabel(i,str(etas[i-1])+"-"+str(etas[i]))

    for j in range(1,9):
       CorrectedRateDATA.GetXaxis().SetBinLabel(j,str(pts[j-1])+"-"+str(pts[j]))
       CorrectedRateMC.GetXaxis().SetBinLabel(j,str(pts[j-1])+"-"+str(pts[j]))
       RawRateDATA.GetXaxis().SetBinLabel(j,str(pts[j-1])+"-"+str(pts[j]))
       RawRateMC.GetXaxis().SetBinLabel(j,str(pts[j-1])+"-"+str(pts[j]))
    for i in range(1,7):
    #  for j in range(1,9): 
           RawRateDATA1D.GetXaxis().SetBinLabel(i*8-7,str(etas[i-1])) 
               
    c1 = TCanvas("c1","c1",1000,750)
    c1.SetRightMargin(5)
    #CorrectedRateDATA.SetNdivisions(3)
    #CorrectedRateDATA.GetXaxis().SetRangeUser(25,150)
    CorrectedRateDATA.Draw("colz")
    CorrectedRateDATA.Draw("textsame")
    c1.Print("CorrectedRateDATA.pdf")
    RawRateDATA.Draw("colz")
    RawRateDATA.Draw("textsame")
    c1.Print("RawRateDATA.pdf")
    RawRateMC.Draw("colz")
    RawRateMC.Draw("textsame")
    c1.Print("RawRateMC.pdf")
    CorrectedRateMC.Draw("colz")
    CorrectedRateMC.Draw("textsame")
    c1.Print("CorrectedRateMC.pdf")
    c1.SetLogy()


    RawRateDATA1D.SetMarkerColor(kBlack)
    RawRateMC1D.SetMarkerColor(kBlue)
    CorrectedRateDATA1D.SetMarkerColor(kGreen)
    CorrectedRateMC1D.SetMarkerColor(kRed)
    RawRateDATA1D.SetLineColor(kBlack)
    RawRateMC1D.SetLineColor(kBlue)
    CorrectedRateDATA1D.SetLineColor(kGreen)
    CorrectedRateMC1D.SetLineColor(kRed)

    Leg1 = TLegend(0.20,0.70,0.44,0.92)    
    Leg1.AddEntry(RawRateDATA1D,"Raw DATA Rates","l")
    Leg1.AddEntry(RawRateMC1D,"Raw MC Rates","l")
    Leg1.AddEntry(CorrectedRateDATA1D,"Corrected DATA Rates","l")
    Leg1.AddEntry(CorrectedRateMC1D,"Corrected MC Rates","l")
    RawRateDATA1D.Draw("hist")
    RawRateMC1D.Draw("histsame")
    CorrectedRateDATA1D.Draw("histsame")
    CorrectedRateMC1D.Draw("histsame")
    line1 = TLine(8,0,8,0.2)
    line1.Draw("same")
    line2 = TLine(16,0,16,0.2)
    line2.Draw("same")
    line3 = TLine(24,0,24,0.2)
    line3.Draw("same")
    line4 = TLine(32,0,32,0.2)
    line4.Draw("same")
    line5 = TLine(40,0,40,0.2)
    line5.Draw("same")
    Leg1.Draw("same")
    c1.Print("1d.pdf")
   
#1D part










    return
main()

