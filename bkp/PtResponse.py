from ROOT import *
import math
def main():
    RATIO = "kkk"
    SCALE = "ppp"
    gROOT.LoadMacro("AtlasStyle.C")
    SetAtlasStyle()
    gROOT.SetBatch(1)
    gStyle.SetLineWidth(1)
    gStyle.SetMarkerSize(1)
    gStyle.SetOptStat(False)
    inputfile  = TFile("/sps/atlas/h/hanlin/QMISID/batchOutput/SherpaMC17_TightT.root",'READ')
    h1r = inputfile.Get("h_el_pt_RightCharged").Clone()
    h2r = inputfile.Get("h_el_Et_RightCharged").Clone()
    h3r = inputfile.Get("h_el_pt_RightCharged_Truth").Clone()
   
    h1w = inputfile.Get("h_el_pt_WrongCharged").Clone()
    h2w = inputfile.Get("h_el_Et_WrongCharged").Clone()
    h3w = inputfile.Get("h_el_pt_WrongCharged_Truth").Clone()
   
    h2dw = inputfile.Get("h_el_PT_response_wrong").Clone()
    h2dr = inputfile.Get("h_el_PT_response_right").Clone()
    c1 = TCanvas("c1","c1",1000,750)
    Leg1 = TLegend(0.65,0.50,0.94,0.92)
    #tex =  TLatex(0.2,0.8,"#font[42]{Right Charged Electrons}");
    c1.cd()
    h1r.GetXaxis().SetRangeUser(25,80)
    h1r.GetXaxis().SetTitle("P_{T}[GeV]")
    h1r.Scale(1./h1r.Integral())
    h1r.GetYaxis().SetRangeUser(0.,0.065)
    h1r.SetMarkerColor(kBlue)
    h1r.SetLineColor(kBlue)
    h1r.GetYaxis().SetTitle("Normalized Events")
    h1r.Draw("e")
    h3r.Scale(1./h3r.Integral())
    h3r.Draw("histsame")
    Leg1.AddEntry(h1r,"Right Charged","")
    Leg1.AddEntry(h1r,"Reco P_{T}","apl")
    Leg1.AddEntry(h3r,"Truth P_{T}","apl")
    Leg1.Draw("same")
    #tex.Draw()
    c1.Print("oneDPTplotsRight.pdf")


    Leg2 = TLegend(0.65,0.50,0.94,0.92)
    #h1w.GetYaxis().SetRangeUser(0.,0.08)
    h1w.GetXaxis().SetRangeUser(25,80)
    h1w.GetXaxis().SetTitle("P_{T}[GeV]")
    h1w.GetYaxis().SetTitle("Normalized Events")


    h1w.Scale(1./h1w.Integral())
    h1w.GetYaxis().SetRangeUser(0.,0.065)
    h3w.Scale(1./h3w.Integral())
    h1w.SetMarkerColor(kRed)
    h1w.SetLineColor(kRed)
    h1w.Draw("e")
    h3w.SetMarkerColor(kGreen)
    h3w.SetLineColor(kGreen)
    Leg2.AddEntry(h2w,"Wrong Charged","")
    Leg2.AddEntry(h1w,"Reco P_{T}","apl")
    Leg2.AddEntry(h3w,"Truth P_{T}","apl")
    Leg2.Draw("same")
    h3w.Draw("histsame")
    c1.Print("oneDPTplotsWrongS.pdf")
    c1.SetRightMargin(8)
    #c1.SetLeftMargin(1)
    h2dw.GetXaxis().SetRangeUser(25,80)
    h2dw.GetYaxis().SetRangeUser(25,80)
    h2dw.GetXaxis().SetTitle("P_{T}^{Reco}[GeV]")
    h2dw.GetYaxis().SetTitle("P_{T}^{Truth}[GeV]")

    h2dr.GetXaxis().SetRangeUser(25,80)
    h2dr.GetYaxis().SetRangeUser(25,80)
    h2dr.GetXaxis().SetTitle("P_{T}^{Reco}[GeV]")
    h2dr.GetYaxis().SetTitle("P_{T}^{Truth}[GeV]")
    h2dw.Draw("colz")
    c1.Print("twoDPTplotsWrongS.pdf")
    h2dr.Draw("colz")
    c1.Print("twoDPTplotsRightS.pdf")











    return
main()

