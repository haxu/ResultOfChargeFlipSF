from ROOT import *
import math
def main():
    RATIO = "kkk"
    SCALE = "ppp"
    gROOT.LoadMacro("AtlasStyle.C")
    SetAtlasStyle()
    gROOT.SetBatch(1)
    gStyle.SetLineWidth(1)
    gStyle.SetMarkerSize(1)
    gStyle.SetOptStat(False)
    gStyle.SetPaintTextFormat("1.4f");
    gStyle.SetErrorX(0) 
    nominalDATA  = TFile("/afs/in2p3.fr/home/h/hxu/QMISID/EGRateMeasure/For_egamma_Truth/All17/Output_DATA_TightT.root",'READ')
    nominalMC  = TFile("/afs/in2p3.fr/home/h/hxu/QMISID/EGRateMeasure/For_egamma_Truth/All17/Output_MC_TightT.root",'READ')
    TruthMC  = TFile("/afs/in2p3.fr/home/h/hxu/QMISID/EGRateMeasure/For_egamma_Truth/MCLHvariation/All17_LH/Output_mc17_TightT.root",'READ')
    bkgsubDATA  = TFile("/afs/in2p3.fr/home/h/hxu/QMISID/EGRateMeasure/For_egamma_Truth/All17_bkgsub/Output_DATA_TightT.root",'READ')
    bkgsubMC  = TFile("/afs/in2p3.fr/home/h/hxu/QMISID/EGRateMeasure/For_egamma_Truth/All17_bkgsub/Output_MC_TightT.root",'READ')
    massDATA = TFile("/afs/in2p3.fr/home/h/hxu/QMISID/EGRateMeasure/For_egamma_Truth/All17_mass/Output_DATA_TightT.root",'READ')
    massMC = TFile("/afs/in2p3.fr/home/h/hxu/QMISID/EGRateMeasure/For_egamma_Truth/All17_mass/Output_MC_TightT.root",'READ')
    triggerDATA = TFile("/afs/in2p3.fr/home/h/hxu/QMISID/EGRateMeasure/For_egamma_Truth/All17_trigR/Output_DATA_TightT.root",'READ')   
    triggerMC = TFile("/afs/in2p3.fr/home/h/hxu/QMISID/EGRateMeasure/For_egamma_Truth/All17_trigR/Output_MC_TightT.root",'READ')
   #sample dependence  
    SDDATA = TFile("/afs/in2p3.fr/home/h/hxu/QMISID/EGRateMeasure/For_egamma_Truth/All17_MatrixDependance/Output_DATA_TightT.root",'READ')
    SDMC = TFile("/afs/in2p3.fr/home/h/hxu/QMISID/EGRateMeasure/For_egamma_Truth/All17_MatrixDependance/Output_MC_TightT.root",'READ')
    etas = [0.0, 0.6, 1.1, 1.52, 1.7, 2.3, 2.5]
    pts  = [25.0,30.0,40.0,50.0,60.0,70.0, 95.0, 130.0, 1000.0]


   #Raw Check
    RawMass = TFile("/afs/in2p3.fr/home/h/hxu/QMISID/EGRateMeasure/For_egamma_Truth/All17_mass/70.000000_110.000000_0.000000_0.000000_DATA_TightT.root",'READ')
    RawNominal = TFile("/afs/in2p3.fr/home/h/hxu/QMISID/EGRateMeasure/For_egamma_Truth/All17/80.000000_100.000000_0.000000_0.000000_DATA_TightT.root",'READ')
    RawMC= TFile("/afs/in2p3.fr/home/h/hxu/QMISID/EGRateMeasure/For_egamma_Truth/All17/80.000000_100.000000_0.000000_0.000000_MC_TightT.root",'READ')
    h_RawMass = RawMass.Get("70.0_110.0_0.0_0.0_DATA_TightT_misid").Clone()
    h_RawNominal = RawNominal.Get("80.0_100.0_0.0_0.0_DATA_TightT_misid").Clone()
    h_RawMC = RawMC.Get("80.0_100.0_0.0_0.0_MC_TightT_misid").Clone()
    
    TruthMCraw =TFile("/afs/in2p3.fr/home/h/hxu/QMISID/EGRateMeasure/For_egamma_Truth/MCLHvariation/All17_LH/Raw_mc17_TightT.root",'READ')
    h_TruthMCraw = TruthMCraw.Get("80.0_100.0_0.0_0.0_mc17_TightT_misid").Clone()
    
    h_nominalDATA  =  nominalDATA.Get("80.0_100.0_0.0_0.0_DATA_TightT_Correctedmisid").Clone()
    h_nominalMC  =  nominalMC.Get("80.0_100.0_0.0_0.0_MC_TightT_Correctedmisid").Clone()
    h_TruthMC = TruthMC.Get("80.0_100.0_0.0_0.0_mc17_TightT_Correctedmisid").Clone()
    h_bkgsubDATA = bkgsubDATA.Get("80.0_100.0_20.0_20.0_DATA_TightT_Correctedmisid").Clone()
    h_bkgsubMC = bkgsubMC.Get("80.0_100.0_20.0_20.0_MC_TightT_Correctedmisid").Clone()
    h_massDATA = massDATA.Get("80.0_100.0_0.0_0.0_DATA_TightT_Correctedmisid").Clone()
    h_massMC = massMC.Get("80.0_100.0_0.0_0.0_MC_TightT_Correctedmisid").Clone()

    h_triggerDATA = triggerDATA.Get("80.0_100.0_0.0_0.0_DATA_TightT_Correctedmisid").Clone()
    h_triggerMC = triggerMC.Get("80.0_100.0_0.0_0.0_MC_TightT_Correctedmisid").Clone()
    
    h_SDDATA = SDDATA.Get("80.0_100.0_0.0_0.0_DATA_TightT_Correctedmisid").Clone()
    h_SDMC = SDMC.Get("80.0_100.0_0.0_0.0_MC_TightT_Correctedmisid").Clone() 
    #h_nominalDATA  =  nominalDATA.Get("80.0_100.0_0.0_0.0_DATA_TightT_Correctedmisid").Clone() 
    SFnominal = h_nominalDATA.Clone()
    SFnominal.Divide(h_nominalMC)

    SFTruth = h_nominalDATA.Clone()
    SFTruth.Divide(h_TruthMC)
    
    SFTrig = h_triggerDATA.Clone()
    SFTrig.Divide(h_triggerMC)
    
    SFSD = h_SDDATA.Clone()
    SFSD.Divide(h_nominalMC)
    
    SFDT = h_SDDATA.Clone()
    SFDT.Divide(h_nominalDATA)
    
    SFMC = h_SDMC.Clone()
    SFMC.Divide(h_nominalMC)
     
    SFbkgsub = h_bkgsubDATA.Clone()
    SFbkgsub.Divide(h_bkgsubMC)
    
    SFmass = h_massDATA.Clone()
    SFmass.Divide(h_massMC)
    #for i in range(1,7):
    #    for j in range(1,9):
    #        SFmass.SetBinContent(j,i,math.fabs(SFmass.GetBinContent(j,i)))
    #SFmass.Divide(h_nominalMC)
    

    SFstat = SFnominal.Clone()
    for i in range(1,7):
       SFnominal.GetYaxis().SetBinLabel(i,str(etas[i-1])+"-"+str(etas[i]))
       for j in range(1,9):
          SFstat.SetBinContent(j,i,SFnominal.GetBinError(j,i))
    for j in range(1,9):
       SFnominal.GetXaxis().SetBinLabel(j,str(pts[j-1])+"-"+str(pts[j]))
    
    c1 = TCanvas("c1","c1",1000,750)
    c1.SetRightMargin(5)
    SFnominal.GetXaxis().SetTitle("P_{T}[GeV]")
    SFnominal.Draw("colz")
    
    SFnominal.Draw("textsame")
    SFnominal.SetBins(8,0,8,6,0,6)
    SFmass.SetBins(8,0,8,6,0,6)
    #SFnominal.GetXaxis().SetRangeUser(25,130)
    c1.Print("SFnominal.pdf")
    SFstat.SetBins(8,0,8,6,0,6)
    SFstat.Divide(SFnominal)
    SFstat.Draw("colz")
    SFstat.Draw("textsame")
    c1.Print("SFstat.pdf")
     
    
    SFmass.Add(SFnominal,-1)
    SFmass.Divide(SFnominal)
    SFmass.Scale(100.)
    for i in range(1,7):
       SFmass.GetYaxis().SetBinLabel(i,str(etas[i-1])+"-"+str(etas[i]))
       for j in range(1,9):
         SFmass.GetXaxis().SetBinLabel(j,str(pts[j-1])+"-"+str(pts[j]))
         SFmass.SetBinContent(j,i,math.fabs(SFmass.GetBinContent(j,i)))

    SFmass.Draw("colz")
    SFmass.Draw("textsame") 
    c1.Print("Systmass.pdf")
    
    SFTruth.SetBins(8,0,8,6,0,6)
    SFTruth.Add(SFnominal,-1)
    SFTruth.Divide(SFnominal)
    for i in range(1,7):
       SFTruth.GetYaxis().SetBinLabel(i,str(etas[i-1])+"-"+str(etas[i]))
       for j in range(1,9):
         SFTruth.GetXaxis().SetBinLabel(j,str(pts[j-1])+"-"+str(pts[j]))
         SFTruth.SetBinContent(j,i,math.fabs(SFTruth.GetBinContent(j,i)))
    SFTruth.Scale(100.)
    SFTruth.Draw("colz")
    SFTruth.Draw("textsame")
    c1.Print("SystTruth.pdf") 
    SFTrig.SetBins(8,0,8,6,0,6)
    SFTrig.Add(SFnominal,-1)
    SFTrig.Divide(SFnominal)
    SFTrig.Scale(100.)
    for i in range(1,7):
       SFTrig.GetYaxis().SetBinLabel(i,str(etas[i-1])+"-"+str(etas[i]))
       for j in range(1,9):
         SFTrig.GetXaxis().SetBinLabel(j,str(pts[j-1])+"-"+str(pts[j]))
         SFTrig.SetBinContent(j,i,math.fabs(SFTrig.GetBinContent(j,i)))

    SFTrig.Draw("colz")
    SFTrig.Draw("textsame")
  
    c1.Print("SystTrig.pdf")
       
    SFSD.SetBins(8,0,8,6,0,6)
    SFSD.Draw("colz")
    SFSD.Draw("textsame")

    c1.Print("SystSD.pdf")
    SFMC.SetBins(8,0,8,6,0,6)
    SFMC.Draw("colz")
    SFMC.Draw("textsame")

    c1.Print("SystMC.pdf")
    SFDT.SetBins(8,0,8,6,0,6)
    SFDT.Draw("colz")
    SFDT.Draw("textsame")

    c1.Print("SystDT.pdf")
   
    h_RawMC.Divide(h_TruthMCraw)   
    h_RawMC.SetBins(6,0,6,8,0,8)
    h_RawMC.Draw("colz")
    h_RawMC.Draw("textsame")
    c1.Print("RawCheck.pdf")
  
    h_RawNominal.Divide(h_RawMC)
    h_RawNominal.SetBins(6,0,6,8,0,8)
    h_RawNominal.Draw("colz")
    h_RawNominal.Draw("textsame")
    c1.Print("RawCheck2.pdf")
    SFbkgsub.SetBins(8,0,8,6,0,6)
    SFbkgsub.Add(SFnominal,-1)
    SFbkgsub.Divide(SFnominal)
    SFbkgsub.Scale(100.)
    for i in range(1,7):
       SFbkgsub.GetYaxis().SetBinLabel(i,str(etas[i-1])+"-"+str(etas[i])) 
       for j in range(1,9):
         SFbkgsub.GetXaxis().SetBinLabel(j,str(pts[j-1])+"-"+str(pts[j]))
         SFbkgsub.SetBinContent(j,i,math.fabs(SFbkgsub.GetBinContent(j,i)))
    SFbkgsub.Draw("colz")
    SFbkgsub.Draw("textsame")
    c1.Print("bkgsub.pdf")

   
   











    return
main()

