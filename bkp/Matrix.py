from ROOT import *
import math
def main():
    RATIO = "kkk"
    SCALE = "ppp"
    gROOT.LoadMacro("AtlasStyle.C")
    SetAtlasStyle()
    gROOT.SetBatch(1)
    gStyle.SetLineWidth(1)
    gStyle.SetMarkerSize(1)
    gStyle.SetOptStat(False)
    inputfile  = TFile("/afs/in2p3.fr/home/h/hxu/QMISID/EnergyCorrection/ResultMatrix/SherpaMC17_TightT_MatrixOutput.root",'READ')
    h2dw = inputfile.Get("Matrix2DWrong_0.00").Clone()
    h2dr = inputfile.Get("Matrix2DRightReverse_0.00").Clone()
    c1 = TCanvas("c1","c1",1000,750)

    h2dw.GetXaxis().SetRangeUser(25,150)
    h2dw.GetYaxis().SetRangeUser(25,150)
    h2dw.GetXaxis().SetTitle("P_{T}^{Reco}[GeV]")
    h2dw.GetYaxis().SetTitle("P_{T}^{Truth}[GeV]")

    h2dr.GetXaxis().SetRangeUser(25,150)
    h2dr.GetYaxis().SetRangeUser(25,150)
    h2dr.GetYaxis().SetTitle("P_{T}^{Reco}[GeV]")
    h2dr.GetXaxis().SetTitle("P_{T}^{Truth}[GeV]")
    h2dw.Draw("colz")
    h2dw.Draw("textsame")
    c1.Print("MatrixWrongS.pdf")
    h2dr.Draw("colz")
    h2dr.Draw("textsame")
    c1.Print("MatrixRightRS.pdf")











    return
main()

